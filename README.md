# Octarine test

This is a test

# REST API

There are two methods available

## Generate new random number

### Request

`POST /api/random-number/generate`

    curl -i -H 'Accept: application/json' -d 'name=Foo&status=new' http://localhost/api/random-number/generate

### Response

    HTTP/1.1 201 Created
    Date: ...
    Status: 201 Created
    Connection: close
    Content-Type: application/json
    Location: /api/random-number/retrieve/1
    Content-Length: ...

    {"id":1,"value":"1234567"}

## Get a specific random number

### Request

`GET /api/random-number/retrieve/id`

    curl -i -H 'Accept: application/json' http://localhost/api/random-number/retrieve/1

### Response

    HTTP/1.1 200 OK
    Date: ...
    Status: 200 OK
    Connection: close
    Content-Type: application/json
    Content-Length: ...

    {"id":1,"value":"1234567"}

## License

The test is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
