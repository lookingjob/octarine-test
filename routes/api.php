<?php

use App\Models\RandomNumber;
use App\Http\Controllers\RandomNumberController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Generate a random number
Route::post('random-number/generate', [RandomNumberController::class, 'generate']);

// Retrieve the random number
Route::get('random-number/retrieve/{randomNumber}', [RandomNumberController::class, 'retrieve']);

