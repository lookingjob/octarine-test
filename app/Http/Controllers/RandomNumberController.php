<?php

namespace App\Http\Controllers;

use App\Models\RandomNumber;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * RandomNumberController class.
 */
class RandomNumberController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function generate(Request $request)
    {
        $model = new RandomNumber([
            'value' => rand(),
        ]);

        return response()->json($model, 201);
    }

    /**
     * Retrieve the specified random number.
     *
     * @param RandomNumber $randomNumber
     * @return RandomNumber
     */
    public function retrieve(RandomNumber $randomNumber): RandomNumber
    {
        return $randomNumber;
    }
}
