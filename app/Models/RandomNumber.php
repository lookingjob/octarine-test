<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * RandomNumber class.
 */
class RandomNumber extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = ['value'];
    /**
     * @var string[]
     */
    protected $visible = [
        'id',
        'value',
    ];
}
